let selectedRow = null;

function onFormSubmit(event) {
    event.preventDefault();
    let listData = readData();
    if (selectedRow == null) {
        insertNewData(listData);
    } else {
        updateRecord(listData);

    }
    resetForm();

}

function readData() {
    let listData = {};
    listData["namesOfFood"] = document.getElementById("namesOfFood").value;
    return listData;
}

function insertNewData(data) {
    let table = document.getElementById("foodlist").getElementsByTagName("tbody")[0];
    let newRow = table.insertRow(table.length);
    let cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.namesOfFood;
    let cell2 = newRow.insertCell(1);
    cell2.innerHTML = `<button onClick='onEdit(this)'> Edit</button>
                       <button onClick='onDelete(this)'> Delete </button>`
}

function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("namesOfFood").value = selectedRow.cells[0].innerHTML;
}

function updateRecord(listData) {
    selectedRow.cells[0].innerHTML = listData.namesOfFood;
}


function onDelete(td) {
    if (confirm("Are you sure to delete this record?")) {
        row = td.parentElement.parentElement;
        document.getElementById("foodlist").deleteRow(row.rowIndex);
    }
    resetForm();
}

function resetForm() {
    document.getElementById("namesOfFood").value = " ";
}